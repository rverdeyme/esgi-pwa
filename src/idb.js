import { openDB } from 'idb';

export async function initDB() {
    const config = {"version": 1};

    const db = await openDB('awesome-todo', config.version || 1, {
        upgrade(db) {
            const store = db.createObjectStore('todos', {
                keyPath: 'id'
            });

            // Create indexes
            store.createIndex('id', 'id');
            store.createIndex('hour', 'hour');
            store.createIndex('name', 'name');
            store.createIndex('deleted', 'deleted');
        }
    });
    return db;
}

export async function setTodos(data) {
    const db = await initDB();
    const tx = db.transaction('todos', 'readwrite');
    data.forEach(item => {
        tx.store.put(item);
    });
    await tx.done;
    return await db.getAllFromIndex('todos', 'id');
}

export async function setTodo(data) {
    const db = await initDB();
    const tx = db.transaction('todos', 'readwrite');
    await tx.store.put(data);
    return await db.getAllFromIndex('todos', 'id');
}

export async function getTodos() {
    const db = await initDB();
    return await db.getAllFromIndex('todos', 'id');
}

export async function getTodo(id) {
    const db = await initDB();
    return await db.getFromIndex('todos', 'id', Number(id));
}