// Import
import page from 'page';
import { setTodos, setTodo, getTodos, getTodo } from './idb.js';

// Get element #root
const app = document.querySelector('#root');

// Initialize var
let connection;
let addOpened = false;

// Get connection status
if (navigator.connection.downlink > 0) {
    connection = true;
} else {
    connection = false;
}

// Page : List
page('/', async (ctx) => {

    // Load and render page
    const module = await import('./views/list.js');
    const List = module.default;
    const ctn = app.querySelector('[page="list"]');
    const ListView = new List(ctn);
    ctn.classList.add('visible');

    if (app.getElementsByClassName('todo').length == 0 && connection) {

        updateDb();

        // Get todos
        axios.get('http://localhost:3000/todos')
        .then(resp => {

            // Set todos
            setTodos(resp.data);

            // Render todo (if no todo -> "No todo yet..")
            if (resp.data.length > 0) {
                resp.data.forEach(element => {
                    addTodoToRender(element);
                });
            } else {
                noTodoToRender();
            }

            addEvents();
        })
        .catch(error => {
            console.log(error);
        }); 

    } else if (app.getElementsByClassName('todo').length == 0 && connection == false) {
        // Get Todos from IDB
        let todos = await getTodos() || [];

        if (todos.length > 0) {
            todos.forEach(element => {
                addTodoToRender(element);
            });
        } else {
            noTodoToRender();
        }

        addEvents();
    }
});

// Page : Todo
page('/:id', async (ctx) => {
    if (ctx.params.id >= 0) {

        // Load and render page
        const module = await import('./views/todo.js');
        const Todo = module.default;
        const ctn = app.querySelector('[page="todo"]');
        const TodoView = new Todo(ctn);
        ctn.classList.add('visible');

        // Create redirect to list
        app.querySelector('.return').addEventListener('click', () => {
            ctn.classList.remove('visible');
            page('/');
        });

        // Clear icon render & name
        if (ctn.querySelector('.icon'))
            ctn.querySelector('.icon').remove();
        ctn.querySelector('h1').innerHTML = "";

        // Load todo's icon
        let newIcon = document.createElement('i');
        newIcon.classList.add('icon');
        newIcon.classList.add(getRandomIcon());
        ctn.querySelector('.todoIcon').appendChild(newIcon);

        if(connection) {
            axios.get('http://localhost:3000/todos/' + ctx.params.id)
            .then(resp => {
                // Load todo's name
                ctn.querySelector('h1').innerHTML = resp.data.name;
            })
            .catch(error => {
                console.log(error);
            }); 
        } else {
            // Load local todo with ID
            let newTodo = await getTodo(ctx.params.id);

            // Load todo's name
            ctn.querySelector('h1').innerHTML = newTodo.name;
        }
    }
});

// Load rooter
page();

// Function to get random icon
function getRandomIcon() {
    let random = Math.floor(Math.random() * 13);
    switch(random){
        case 0:
            return 'gg-airplane';
        case 1:
            return 'gg-battery-empty';
        case 2:
            return 'gg-awards';
        case 3:
            return 'gg-bee';
        case 4:
            return 'gg-box';
        case 5:
            return 'gg-briefcase';
        case 6:
            return 'gg-calculator';
        case 7:
            return 'gg-card-hearts';
        case 8:
            return 'gg-clapper-board';
        case 9:
            return 'gg-community';
        case 10:
            return 'gg-credit-card';
        case 11:
            return 'gg-dice-1';
        case 12:
            return 'gg-crown';
    }
}

// Add events
function addEvents() {
    setTimeout(() => {
        // Add event : onClick on the add button
        app.querySelector('.add').addEventListener('click', () => {
            if (addOpened == false) {
                addOpened = true;
                app.querySelector('.add').classList.toggle('active');
                setTimeout(() => {
                    app.querySelector('.add input').focus();
                }, 400);
            }
        });

        // Add event : onPress enter when adding a todo
        app.querySelector('.add input').addEventListener('keypress', async function(e) {
            if (e.key === 'Enter' && addOpened) {
                addOpened = false;
                app.querySelector('.add').classList.toggle('active');

                if (app.querySelector('.add input').value) {
                    
                    // Delete "No todo yet.."
                    if (app.querySelector('.no-todo'))
                        app.querySelector('.no-todo').remove();


                    // Get Todos from IDB
                    let todos = await getTodos() || [];
                    let id = 0;
                    if (todos.length > 0) {
                        todos.forEach(element => {
                            if (element.id > id)
                                id = element.id;
                        });
                    }

                    // Render new todo
                    let newItem = {
                        "id": id + 1,
                        "name": app.querySelector('.add input').value,
                        "hour": moment(new Date()).format('MMM Do'),
                        "delete": 'false'
                    }
                    addTodoToRender(newItem);

                    if (connection) {
                        newItem.update = 'true';
                        setTodo(newItem);

                        // Post new todo
                        axios.post('http://localhost:3000/todos',{
                            id: newItem.id,
                            name: app.querySelector('.add input').value,
                            hour: moment(new Date()).format('MMM Do'),
                            deleted: 'false',
                            update: 'true'
                        }).then(resp => {
                            // Clear form
                            app.querySelector('.add input').value = "";
                        }).catch(error => {
                            console.log(error);
                        }); 
                    } else {
                        newItem.update = 'false';
                        setTodo(newItem);
                        app.querySelector('.add input').value = "";
                    }
                }
            }
        });
    }, 100);
}

async function updateDb() {
    // Get Todos from IDB
    let todos = await getTodos() || [];

    let newTodosAdded = 0;

    // Test si à jour Update
    if (todos.length > 0) {
        todos.forEach(element => {
            if (element.update == 'false') {
                element.update = 'true';
                setTodo(element);

                // Compteur de nouveaux todos
                newTodosAdded++;
                
                // Post todo
                axios.post('http://localhost:3000/todos',{
                    name: element.name,
                    hour: element.hour,
                    deleted: 'false',
                    update: 'true'
                }).catch(error => {
                    console.log(error);
                }); 
            }
        });
    }

    return newTodosAdded;
}

// Event when connection change
async function onConnectionChange() {
    if (navigator.connection.downlink > 0) {
        connection = true;
        let newTodosAdded = await updateDb();
        let newNotif = document.createElement('div');
        newNotif.classList.add('notif');
        newNotif.classList.add('notif--green');
        newNotif.appendChild(document.createTextNode('Vous êtes maintenant connecté à internet !'));
        document.querySelector('.visible').appendChild(newNotif);
        setTimeout(() => {
            newNotif.remove();
            console.log(newTodosAdded);
            if (newTodosAdded > 0) {
                let newNotifAdded = document.createElement('div');
                newNotifAdded.classList.add('notif');
                newNotifAdded.classList.add('notif--green');
                newNotifAdded.appendChild(document.createTextNode(`Ajout de ${newTodosAdded} todo(s) dans la database`));
                document.querySelector('.visible').appendChild(newNotifAdded);
            }
        }, 3000);
    } else {
        connection = false;
        let newNotif = document.createElement('div');
        newNotif.classList.add('notif');
        newNotif.classList.add('notif--red');
        newNotif.appendChild(document.createTextNode('Vous êtes maintenant déconnecté à internet !'));
        document.querySelector('.visible').appendChild(newNotif);
        setTimeout(() => {
            newNotif.remove();
        }, 3000);
    }
}
navigator.connection.addEventListener('change', onConnectionChange);

// Function to render todo
function addTodoToRender(item) {
    let newTodo = document.createElement('div');
    newTodo.classList.add('todo');
    let newIcon = document.createElement('i');
    newIcon.classList.add('icon');
    newIcon.classList.add(getRandomIcon());
    let newTitle = document.createElement('div');
    newTitle.classList.add('title');
    newTitle.appendChild(document.createTextNode(item.name));
    let newHour = document.createElement('div');
    newHour.classList.add('hour');
    newHour.appendChild(document.createTextNode(item.hour));
    newTodo.appendChild(newIcon);
    newTodo.appendChild(newTitle);
    newTodo.appendChild(newHour);
    app.querySelector('.list').appendChild(newTodo);

    newTodo.addEventListener('click', () => {
        app.querySelector('[page="list"]').classList.remove('visible');
        page('/' + item.id);
    });
}


// Function to render when no todo
function noTodoToRender() {
    let newTodo = document.createElement('div');
    newTodo.classList.add('todo');
    newTodo.classList.add('no-todo');
    let newIcon = document.createElement('i');
    newIcon.classList.add('icon');
    newIcon.classList.add('gg-smile-sad');
    let newTitle = document.createElement('div');
    newTitle.classList.add('title');
    newTitle.appendChild(document.createTextNode("No todo yet.."));
    let newHour = document.createElement('div');
    newHour.classList.add('hour');
    newHour.appendChild(document.createTextNode("Create bellow !"));
    newTodo.appendChild(newIcon);
    newTodo.appendChild(newTitle);
    newTodo.appendChild(newHour);
    app.querySelector('.list').appendChild(newTodo);
}

// Service Worker
if('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js', { scope: '/' })
      .then(function(registration) {
            console.log('Service Worker Registered');
      });

    navigator.serviceWorker.ready.then(function(registration) {
       console.log('Service Worker Ready');
    });
  }