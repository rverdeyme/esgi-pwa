import { html, render } from 'lit-html';

export default class Todo {
    constructor(page) {
      this.page = page;
      this.renderView();
    }

    template() {
        return html`
        <i class="return gg-arrow-left"></i>
        <h1></h1>
        <div class="todoIcon"></div>
        <div class="todoPreview">
            La description blabla, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </div>`;
      }
    
      renderView() {
        const view = this.template();
        render(view, this.page);
      }
}