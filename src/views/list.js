import { html, render } from 'lit-html';

export default class List {
    constructor(page) {
      this.page = page;
      this.renderView();
    }

    template() {
        return html`
        <i class="menu gg-menu-left"></i>
        <h1>Todo List</h1>
        <div class="list">
        </div>
        <div class="add">
            <i class="gg-math-plus"></i>
            <label>
              <span> Faire semblant d'avoir un label hihi </span>
              <input type="text"/>
            </label>
        </div>`;
      }
    
      renderView() {
        const view = this.template();
        render(view, this.page);
      }
}