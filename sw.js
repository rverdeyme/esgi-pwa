const version = "1";
const cacheName = `airhorner-${version}`;
self.addEventListener('install', e => {
  e.waitUntil(
    caches.open(cacheName).then(cache => {
      return cache.addAll([
        `/`,
        `/index.html`,
        `/assets/styles/style.css`,
        `/assets/styles/fonts.css`,
        `/assets/fonts/roboto.woff2`,
        `/assets/styles/gg.css`,
        `/assets/js/axios.js`,
        `/assets/js/moment.js`,
        `/src/app.js`,
        `/src/idb.js`,
        `/src/views/list.js`,
        `/src/views/todo.js`,
        `/favicon.ico`,
        `/node_modules/page/`,
        `/node_modules/page/page.mjs`,
        `/node_modules/idb/`,
        `/node_modules/idb/build/esm/index.js`,
        `/node_modules/idb/build/esm/wrap-idb-value.js`,
        `/node_modules/lit-html/`,
        `/node_modules/lit-html/lit-html.js`,
        `/node_modules/lit-html/lib/default-template-processor.js`,
        `/node_modules/lit-html/lib/template-result.js`,
        `/node_modules/lit-html/lib/dom.js`,
        `/node_modules/lit-html/lib/part.js`,
        `/node_modules/lit-html/lib/template-factory.js`,
        `/node_modules/lit-html/lib/directive.js`,
        `/node_modules/lit-html/lib/parts.js`,
        `/node_modules/lit-html/lib/template-instance.js`,
        `/node_modules/lit-html/lib/render.js`,
        `/node_modules/lit-html/lib/template.js`,
        `/manifest.json`,
        `/images/icons/icon-144x144.png`
      ])
          .then(() => self.skipWaiting());
    })
  );
});

self.addEventListener('activate', event => {
  event.waitUntil(self.clients.claim());
});

self.addEventListener('fetch', event => {
  event.respondWith(
    caches.open(cacheName)
      .then(cache => cache.match(event.request, {ignoreSearch: true}))
      .then(response => {
      return response || fetch(event.request);
    })
  );
});