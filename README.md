<h1 align="center"> PWA TP 👋 </h1>

📦 Installer les dépendences :

```sh
npm install
```

🚀 Lancer la base de données :

```sh
npm run database
```

🔥 Lancer le serveur :

```sh
npm run serve
```

🎉 Lancer la page web : http://localhost:8080/

> ✨ Dev effectué avec comme résolution de responsive : "iPhone X" (Pas de responsive donc vaut mieux rester sûr de l'iPhone X 🙄)